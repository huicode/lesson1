<!-- TOC -->

- [Jhipster: codegen、 internationalization、upgrade 、qa、ci-cd、production、modules、architecture](#jhipster-codegen%E3%80%81-internationalization%E3%80%81upgrade-%E3%80%81qa%E3%80%81ci-cd%E3%80%81production%E3%80%81modules%E3%80%81architecture)
- [Micro_Service:  gateWay, register, configCenter, loadbalance， circuit](#microservice-gateway-register-configcenter-loadbalance%EF%BC%8C-circuit)
- [Jhipster 环境搭建](#jhipster-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA)
    - [本地环境搭建](#%E6%9C%AC%E5%9C%B0%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA)
        - [后端环境](#%E5%90%8E%E7%AB%AF%E7%8E%AF%E5%A2%83)
        - [前端环境](#%E5%89%8D%E7%AB%AF%E7%8E%AF%E5%A2%83)
    - [JHipster Online](#jhipster-online)
    - [Vagrant box](#vagrant-box)
    - [package manage(brew,choco)](#package-managebrewchoco)
    - [docker](#docker)
- [Docker 环境搭建](#docker-%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA)
- [创建 Jhipster 应用](#%E5%88%9B%E5%BB%BA-jhipster-%E5%BA%94%E7%94%A8)
- [代码结构分析](#%E4%BB%A3%E7%A0%81%E7%BB%93%E6%9E%84%E5%88%86%E6%9E%90)

<!-- /TOC -->   

# Jhipster: codegen、 internationalization、upgrade 、qa、ci-cd、production、modules、architecture
# Micro_Service:  gateWay, register, configCenter, loadbalance， circuit

![front](https://upload-images.jianshu.io/upload_images/2178607-75bb3c9accaad76b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![server](https://upload-images.jianshu.io/upload_images/2178607-84726f7abac80c28.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![deploy and ci-cd](https://upload-images.jianshu.io/upload_images/2178607-e33bf8347d0dab1f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
# Jhipster 环境搭建

## 本地环境搭建

### 后端环境
> jdk1.8
> git

### 前端环境
> node  

`brew install nodejs`
> npm

`npm install -g nrm`
`nrm ls`    `nrm use taobao`
`npm install -g n`
`n 8 latest`
> yarn 

`brew install yarn`
`yarn config set registry https://registry.npm.taobao.org`

> yo

`yarn global add yo`
> jhipster

`yarn global add generator-jhipster`

> export PATH="$PATH:`yarn global bin`:$HOME/.config/yarn/global/node_modules/.bin"

## JHipster Online
[https://start.jhipster.tech/](https://start.jhipster.tech/)

## Vagrant box

## package manage(brew,choco)

## docker

docker 环境安装忽略

```sh
docker container run --name jhipster -v ~/jhipster:/home/jhipster/app -v ~/.m2:/home/jhipster/.m2 -p 8080:8080 -p 9000:9000 -p 3001:3001 -d -t jhipster/jhipster
```

`docker container exec -it --user root jhipster bash`


# Docker 环境搭建


# 创建 Jhipster 应用

```setting.xml
<mirror>
    <id>alimaven</id>
    <name>aliyun maven</name>
    <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
    <mirrorOf>central</mirrorOf>
</mirror>
```
```
http://qiniu.huicode.top/soft/phantomjs-2.1.1-macosx.zip
http://qiniu.huicode.top/soft/phantomjs-2.1.1-linux-x86_64.tar.bz2
http://qiniu.huicode.top/soft/phantomjs-2.1.1-windows.zip
```


# 代码结构分析



