/**
 * View Models used by Spring MVC REST controllers.
 */
package top.huicode.sfgg.demo.web.rest.vm;
